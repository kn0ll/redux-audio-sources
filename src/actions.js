import { createAction } from 'redux-actions';
import uuid from 'node-uuid';

export const addAudioSource = createAction('ADD_AUDIO_SOURCE', null, () => ({
  id: uuid.v4()
}));
export const setAudioSourceName = createAction('SET_AUDIO_SOURCE_NAME');