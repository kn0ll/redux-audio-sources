import { Map } from 'immutable';
import { handleActions } from 'redux-actions';

import { addAudioSource, setAudioSourceName } from './actions';

import state from './state';

let audioSourcesReducer = handleActions({

  [addAudioSource]: (state, action) => {
    return state.push(Map({
      id: action.meta.id,
      name: action.payload.name,
      size: action.payload.size,
      buffer: action.payload.buffer
    }));
  },

  [setAudioSourceName]: (state, action) => {
    return state.setIn([state.findIndex((item) => 
      item.get('id') == action.payload.id
    ), 'name'], action.payload.name);
  }

}, state);

export default audioSourcesReducer;