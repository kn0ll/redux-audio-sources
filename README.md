# redux-audio-sources
a collection of [Redux](http://redux.js.org/) actions and reducers to manage an [Immutable](https://facebook.github.io/immutable-js/) list of "audio sources". what is an audio source? while audio applications only need an AudioBuffer to actually play the audio, it's likely your application will want to keep track of some metadata relating to the buffer. an audio source is an Immutable map with an `id`, `name`, `size`, and `buffer`.

[![npm version](https://img.shields.io/npm/v/redux-audio-sources.svg?style=flat-square)](https://www.npmjs.com/package/redux-audio-sources)

- [installation](#installation)
- [integration](#integration)
- [api](#api)

---

## installation
```
npm install redux-audio-sources --save
```

---

## integration

### initialize state

first, we have to add the the default preloaded state provided by redux-audio-sources to our applications preloaded Redux state.

```
import audioSourcesState from 'redux-audio-sources/lib/state';

const preloadedState = Immutable.Map({
  audioSources: audioSourcesState,
  ...yourState
});
```

### register the reducer

next, we have to register the reducer provided by redux-audio-sources with our Redux store.

```
import audioSourcesReducer from 'redux-audio-sources/lib/reducer';
import { combineReducers } from 'redux-immutable';

const reducer = combineReducers({
  audioSources: audioSourcesReducer,
  ...yourReducers
});
```

### connect the actions and state to your view

finally, we can connect the actions provided by redux-audio-sources to our [React](https://facebook.github.io/react/) components.

```
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { setAudioSourceName } from 'redux-audio-sources/lib/actions';

const mapStateToProps = (state) => {
  return {
    audioSources: state.get('audioSources')
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setAudioSourceName: (id, name) => {
      dispatch(setAudioSourceName({ id, name }));
    }
  };
};

@connect(mapStateToProps, mapDispatchToProps)
class AudioSourcesList extends PureComponent {

  render() {
    const { audioSources, setAudioSourceName } = this.props;
    
    return (
      <ul>
        {audioSources.map((source) => {
          const id = source.get('id');
          const name = source.get('name');
          
          return (
            <li>
              name: {name}<br />
              <input
                type="text"
                defaultValue={name}
                onKeyPress={(e) => {
                  setAudioSourceName(id, e.target.value);
                }} />
            </li>
          );
        })}
      </ul>
    );
  }

}
```

---

## api

### actions

- `addAudioSource` given a payload containing `name`, `size`, and `buffer`, adds an audio source to the list of audio sources.
- `setAudioSourceName` given a payload of `id` and `name`, changes an audio sources name.